package com.pbi.test.di.component

import com.pbi.test.PBIApplication
import com.pbi.test.di.modules.AppModule
import com.pbi.test.di.modules.BuildersModule
import com.pbi.test.di.modules.NetModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(AndroidInjectionModule::class, BuildersModule::class, AppModule::class,
        NetModule::class)
)
interface AppComponent {
  fun inject(app: PBIApplication)
}