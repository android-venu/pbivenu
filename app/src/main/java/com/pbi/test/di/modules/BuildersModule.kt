package com.pbi.test.di.modules

import com.pbi.test.ui.NYTimesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

  @ContributesAndroidInjector
  abstract fun contributesNYTimesActivity(): NYTimesActivity
}